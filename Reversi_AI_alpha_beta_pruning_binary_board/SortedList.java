package beat;

public class SortedList {
    /**
     * Inserts a value into a sorted list so that the resulting list is still sorted.
     * The sort order is ascending.
     *
     * @param r  a sorted list.
     *              After the operation the state of this list is undefined.
     *              Don't use it any more. Use the returned list instead.
     * @param value the value which is inserted into the list
     * @return
     */
    private static LinkedList insertSorted (LinkedList r, int index, float value) {
        if (r == null)
            return new LinkedList(index, value, null);    //if there si no list, create new List and put the value in there
        if (r.value > value)
            r.next = insertSorted(r.next, index, value);     //until value is greater than list.value, look for the next list.value and compare with value
        if (r.value <= value)
            return new LinkedList(index, value, r);      //once you find some list.value greater than value, return new List containing value and connect the last list with it
        return r;
    }

    /**
     * Sorts a r in ascending order.
     *
     * @param r the r which is sorted.
     *             After the operation the state of this r is undefined.
     *             Don't use it any more. Use the returned r instead.
     * @return the sorted variant of the given r
     */
    public static LinkedList sort (LinkedList r) {
        if (LinkedList.numberOfNodes(r) <= 1) return r;   //if there is only up to 1 element, no need to sort anything, or this is recursion end
        LinkedList sortedList = sort (r.next);  //sortedList is always sorted, at start it represents the last r. r is always one r before sortedList
        return insertSorted(sortedList, r.index, r.value); //therefore we can sort r via insertSorted using sortedList and r.value. We sort the whole r from end to start. clever :)

    }
}
