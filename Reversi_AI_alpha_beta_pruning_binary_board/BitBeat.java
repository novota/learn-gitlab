package beat;

import reversi.*;
/*
change log
count the stone difference in late game instead of using whoWins function
zero window search
/4 for inside board stability
 */
public class BitBeat implements ReversiPlayer {
    private long timeOut;
    private long timeBuffer;
    public static int player;
    public static int opponent;
    private long timeLimit;
    private long one; //one which can be shifted
    private long counter = 0;
    public static int[] bonusPoints;
    public static int[] staticEvaluationPoints;

    public BitBeat() {
        bonusPoints = new int[64];
        staticEvaluationPoints = new int[64];
        one = 1L;
    }

    class Timeout extends Throwable {
        private static final long serialVersionUID = 1L;
    }

    @Override
    public void initialize(int player, long timeout) {
        BitBeat.player = player;
        opponent = Utils.other(player);
        this.timeLimit = timeout;
        timeBuffer = 10;
        initializeBonusPoints();
        initializeStaticPoints();
    }

    @Override
    public Coordinates nextMove(GameBoard gameBoard) { //todo: implementovat vyhodnoceni mobility, predevsim v early-mid game je vysoka mobility hodne dulezita
        timeOut = System.currentTimeMillis() + timeLimit - timeBuffer;
        int depth;
        float alpha = -10000;
        float beta = 10000;
        float result;
        int tempBestIndex = 0;
        int bestIndex = 0;
        int counter = 1;

        BitBoard bitBoard = new BitBoard();
        bitBoard.updateBoard(gameBoard);
        int stonesPlayed = bitBoard.stonesPlayed();

        long viableMoves = bitBoard.viableMoves(player);
        LinkedList sortedMoves = SortedList.sort(LinkedList.listOfIndexes(viableMoves, 0));
        LinkedList backupMoves = sortedMoves;
        LinkedList moreBetterMoves = null;

        if (sortedMoves != null) {
            if (stonesPlayed < 49) {
                try {
                    for (int i = 6; ; i++) {
                        if (i == 6){
                            System.out.println(i);
                            while (sortedMoves != null) {
                                long aMove = one << sortedMoves.index;
                                BitBoard hypoBoard = bitBoard.cloneBoard();
                                hypoBoard.makeMove(player, aMove);
                                result = betaSearch(alpha, beta, hypoBoard, i);
                                System.out.println("sorting moves result = " + result);
                                if (result > alpha) {
                                    alpha = result;
                                    tempBestIndex = sortedMoves.index;
                                }
                                sortedMoves.value = result;
                                sortedMoves = sortedMoves.next;
                            }
                            backupMoves = SortedList.sort(backupMoves);
                        }
                        else { // depth is > 6
                            while (sortedMoves != null) {
                                if (counter == 1){
                                    long aMove = one << sortedMoves.index;
                                    BitBoard hypoBoard = bitBoard.cloneBoard();
                                    hypoBoard.makeMove(player, aMove);
                                    result = betaSearch(alpha, beta, hypoBoard, i);
                                    alpha = result;
                                    beta = alpha + 0.1f;
                                    tempBestIndex = sortedMoves.index;
                                    sortedMoves = sortedMoves.next;
                                    counter++;
                                }
                                else {
                                    long aMove = one << sortedMoves.index;
                                    BitBoard hypoBoard = bitBoard.cloneBoard();
                                    hypoBoard.makeMove(player, aMove);
                                    result = betaSearch(alpha, beta, hypoBoard, i);
                                    if (result == beta) {
                                        tempBestIndex = sortedMoves.index;
                                        moreBetterMoves = LinkedList.addIndexToList(sortedMoves.index, moreBetterMoves);
                                    }
                                    sortedMoves = sortedMoves.next;
                                    counter++;
                                }

                            }
                            if (LinkedList.numberOfNodes(moreBetterMoves) > 1){
                                alpha = -10000;
                                beta = 10000;
                                while (moreBetterMoves != null){
                                    long aMove = one << moreBetterMoves.index;
                                    BitBoard hypoBoard = bitBoard.cloneBoard();
                                    hypoBoard.makeMove(player, aMove);
                                    result = betaSearch(alpha, beta, hypoBoard, i);
                                    if (result > alpha){
                                        alpha = result;
                                        tempBestIndex = moreBetterMoves.index;
                                    }
                                    moreBetterMoves = moreBetterMoves.next;
                                }
                            }
                        }
                        counter = 1;
                        sortedMoves = backupMoves;
                        bestIndex = tempBestIndex;
                        alpha = -10000;
                        beta = 10000;
                    }
                } catch (Timeout t) {
                    System.out.println("we timed - outed :)");
                }
            } else {
                depth = 16;
                while (sortedMoves != null) {
                    long aMove = one << sortedMoves.index;
                    BitBoard hypoBoard = bitBoard.cloneBoard();
                    hypoBoard.makeMove(player, aMove);
                    result = betaSearchLate(alpha, beta, hypoBoard, depth);
                    if (result > alpha) {
                        alpha = result;
                        bestIndex = sortedMoves.index;
                    }
                    sortedMoves = sortedMoves.next;
                }
            }
            this.counter = 0;

            return new Coordinates(indexToRow(bestIndex), indexToCol(bestIndex));

        } else return null;
    }

    private float alphaSearch(float alpha, float beta, BitBoard b, int depth) throws Timeout {
        if (System.currentTimeMillis() > timeOut) {
            throw new Timeout();
        }
        counter++;
        depth--;
        if ((depth > 0 && !b.isFull())) { //&& (System.currentTimeMillis() < timeOut)
            long viableMoves = b.viableMoves(player);
            float result;
            LinkedList sortedMoves = SortedList.sort(LinkedList.listOfIndexes(viableMoves, 0));

            if (sortedMoves != null) {
                while (sortedMoves != null) {
                    long aMove = one << sortedMoves.index;
                    BitBoard hypoBoard = b.cloneBoard();
                    hypoBoard.makeMove(player, aMove);
                    result = betaSearch(alpha, beta, hypoBoard, depth);
                    if (result > alpha) {
                        alpha = result;
                    }
                    if (alpha >= beta) {
                        break;
                    }
                    sortedMoves = sortedMoves.next;
                }
                return alpha;
            } else {
                long opponentsMoves = b.viableMoves(opponent);
                if (opponentsMoves == 0) {
                    return whoWins(b);
                } else {
                    return betaSearch(alpha, beta, b, ++depth); //dont count depth for a node with no moves
                }

            }
        }
        else if (b.isFull()){
            return b.countStones(player) - b.countStones(opponent);
        }
        else {
            return b.stabilityBoardEval();
        }
    }

    private float betaSearch(float alpha, float beta, BitBoard b, int depth) throws Timeout {
        if (System.currentTimeMillis() > timeOut) {
            throw new Timeout();
        }
        counter++;
        depth--;

        if ((depth > 0 && !b.isFull())) { // && System.currentTimeMillis() < timeOut
            long viableMoves = b.viableMoves(opponent);
            float result;
            LinkedList sortedMoves = SortedList.sort(LinkedList.listOfIndexes(viableMoves, 0));

            if (sortedMoves != null) {
                while (sortedMoves != null) {
                    long aMove = one << sortedMoves.index;
                    BitBoard hypoBoard = b.cloneBoard();
                    hypoBoard.makeMove(opponent, aMove);
                    result = alphaSearch(alpha, beta, hypoBoard, depth);

                    if (result < beta) {
                        beta = result;
                    }
                    if (alpha >= beta) {
                        break;
                    }
                    sortedMoves = sortedMoves.next;
                }
                return beta;
            } else {
                long opponentMoves = b.viableMoves(player);
                if (opponentMoves == 0) {
                    return whoWins(b);
                } else {
                    return alphaSearch(alpha, beta, b, ++depth); //dont count depth for a node with no moves
                }
            }
        }
        else if (b.isFull()){
            return b.countStones(player) - b.countStones(opponent);
        }
        else {
            return b.stabilityBoardEval();
        }
    }

    private float alphaSearchLate(float alpha, float beta, BitBoard b, int depth) {
        depth--;
        long viableMoves = b.viableMoves(player);
        int nrOfMoves = countOnes(viableMoves);
        int[] moveIndexes = listOfMoves(viableMoves, nrOfMoves);

        if (!b.isFull() && depth != 0 && System.currentTimeMillis() < timeOut) {
            if (nrOfMoves == 0) {
                long viableMovesOpponent = b.viableMoves(opponent);
                if (viableMovesOpponent == 0) {
                    return b.countStones(player) - b.countStones(opponent);
                } else {
                    return betaSearchLate(alpha, beta, b, depth);
                }
            } else {
                float result;
                for (int i = 0; i < moveIndexes.length; i++) { //what if viableMovesOnly == null???

                    long aMove = one << moveIndexes[i];
                    BitBoard hypoBoard = b.cloneBoard();
                    hypoBoard.makeMove(player, aMove);

                    result = betaSearchLate(alpha, beta, hypoBoard, depth);

                    if (result > alpha) {
                        alpha = result;
                    }
                    if (alpha >= beta) {
                        break;
                    }
                }
                return alpha;
            }
        } else {
            return b.countStones(player) - b.countStones(opponent);
        }
    }

    private float betaSearchLate(float alpha, float beta, BitBoard b, int depth) {
        depth--;
        long viableMoves = b.viableMoves(opponent);
        int nrOfMoves = countOnes(viableMoves);
        int[] moveIndexes = listOfMoves(viableMoves, nrOfMoves); //todo: do we really need those indexes?

        if (!b.isFull() && depth != 0 && System.currentTimeMillis() < timeOut) {
            if (nrOfMoves == 0) {
                long viableMovesOpponent = b.viableMoves(player);
                if (viableMovesOpponent == 0) {
                    return b.countStones(player) - b.countStones(opponent);
                } else {
                    return alphaSearchLate(alpha, beta, b, depth);
                }
            } else {
                float result;
                for (int i = 0; i < moveIndexes.length; i++) {

                    long aMove = one << moveIndexes[i];
                    BitBoard hypoBoard = b.cloneBoard();
                    hypoBoard.makeMove(opponent, aMove);

                    result = alphaSearchLate(alpha, beta, hypoBoard, depth);

                    if (result < beta) {
                        beta = result;
                    }
                    if (alpha >= beta) {
                        break;
                    }
                }
                return beta;
            }
        } else {
            return b.countStones(player) - b.countStones(opponent);
        }
    }

    private float whoWins(BitBoard b) {
        if (b.countStones(player) - b.countStones(opponent) > 0) {
            return 300;  //this is a win for us
        } else {
            return -300; //this is a loss or a draw for us
        }
    }

    private static int indexToRow(int index) {
        return (index / 8) + 1;
    }

    private static int indexToCol(int index) {
        return (index % 8) + 1;
    }

    public static int[] listOfMoves(long viableMoves, int nrOfMoves) {
        int arrayIndex = 0;
        int[] listOfMoves = new int[nrOfMoves];
        for (int i = 0; i < 64 && nrOfMoves != 0; i++) {
            if ((viableMoves >> i & 1) == 1) {
                nrOfMoves--;
                listOfMoves[arrayIndex] = i;
                arrayIndex++;
            }
        }
        return listOfMoves;
    }

    public static int countOnes(long number) {
        int counter = 0;
        while (number != 0) {
            number &= (number - 1);
            counter++;
        }
        return counter;
    }

    public static void binaryTostring(long board) {
        for (int i = 0; i < 64; i++) {
            if ((board >> i & 1) == 1) {
                System.out.print(1);
            } else {
                System.out.print(0);
            }
        }
    }

    private void initializeStaticPoints() { //todo: refine bonuses to look for important moves first
        staticEvaluationPoints[0] = 10;
        staticEvaluationPoints[7] = 10;
        staticEvaluationPoints[9] = -2;
        staticEvaluationPoints[14] = -2;
        staticEvaluationPoints[49] = -2;
        staticEvaluationPoints[54] = -2;
        staticEvaluationPoints[56] = 10;
        staticEvaluationPoints[63] = 10;
        /*staticEvaluationPoints[1] = -2;
        staticEvaluationPoints[6] = -2;
        staticEvaluationPoints[8] = -2;
        staticEvaluationPoints[15] = -2;
        staticEvaluationPoints[48] = -2;
        staticEvaluationPoints[55] = -2;
        staticEvaluationPoints[57] = -2;
        staticEvaluationPoints[62] = -2;*/
    }

    private void initializeBonusPoints() {

        bonusPoints[0] = 18;
        bonusPoints[1] = 15;
        bonusPoints[2] = 14;
        bonusPoints[3] = 13;
        bonusPoints[4] = 13;
        bonusPoints[5] = 14;
        bonusPoints[6] = 15;
        bonusPoints[7] = 18;
        bonusPoints[8] = 15;
        bonusPoints[9] = 2;
        bonusPoints[10] = 6;
        bonusPoints[11] = 8;
        bonusPoints[12] = 8;
        bonusPoints[13] = 6;
        bonusPoints[14] = 2;
        bonusPoints[15] = 15;
        bonusPoints[16] = 14;
        bonusPoints[17] = 6;
        bonusPoints[18] = 12;
        bonusPoints[19] = 11;
        bonusPoints[20] = 11;
        bonusPoints[21] = 12;
        bonusPoints[22] = 6;
        bonusPoints[23] = 14;
        bonusPoints[24] = 13;
        bonusPoints[25] = 8;
        bonusPoints[26] = 10;
        bonusPoints[27] = 10;
        bonusPoints[28] = 10;
        bonusPoints[29] = 10;
        bonusPoints[30] = 8;
        bonusPoints[31] = 13;
        bonusPoints[32] = 13;
        bonusPoints[33] = 12;
        bonusPoints[34] = 10;
        bonusPoints[35] = 10;
        bonusPoints[36] = 10;
        bonusPoints[37] = 10;
        bonusPoints[38] = 8;
        bonusPoints[39] = 13;
        bonusPoints[40] = 14;
        bonusPoints[41] = 6;
        bonusPoints[42] = 14;
        bonusPoints[43] = 10;
        bonusPoints[44] = 10;
        bonusPoints[45] = 14;
        bonusPoints[46] = 6;
        bonusPoints[47] = 14;
        bonusPoints[48] = 15;
        bonusPoints[49] = 2;
        bonusPoints[50] = 6;
        bonusPoints[51] = 8;
        bonusPoints[52] = 8;
        bonusPoints[53] = 6;
        bonusPoints[54] = 2;
        bonusPoints[55] = 15;
        bonusPoints[56] = 18;
        bonusPoints[57] = 15;
        bonusPoints[58] = 14;
        bonusPoints[59] = 13;
        bonusPoints[60] = 13;
        bonusPoints[61] = 14;
        bonusPoints[62] = 15;
        bonusPoints[63] = 18;

    }
}
//if bit at index i == false its free on both boards and thus a candidate for a valid mov