from Hlavni_kod.Help_functions import add_kid_to_oddil
from Hlavni_kod.Help_functions import increment_index_tam_a_zpet
from Hlavni_kod.Help_functions import najit_oddil_pro_switch
from Hlavni_kod.Help_functions import vytvor_pocty_kluku_na_oddil
from Hlavni_kod.Help_functions import find_twins
from Hlavni_kod.Help_functions import find_triplet
from Hlavni_kod.Help_functions import neexistuje_dite_bez_oddilu
from Hlavni_kod.Trida_dite import Dite


class Seznam_oddilu:

    def __init__(self, seznam_oddilu =[]):
        self.seznam_oddilu = seznam_oddilu

    def __iadd__(self, other):
        assert isinstance(other, Oddil)
        self.seznam_oddilu.append(other)
        return self

    def __str__(self):
        result = 'Mame nasledujici oddily: \n'
        for element in self.seznam_oddilu:
            result += str(element)
        return result

    def inicializace_oddilu(self, kapacita, pocet, cislo_oddilu):
        while pocet > 0:
            self.seznam_oddilu.append(Oddil(cislo_oddilu, kapacita))
            pocet -= 1
            cislo_oddilu += 1
        return self

    # seradi oddily podle poctu deti v nich od nejmene po nejvice
    def seradit_oddily_vzestupne(self):
        index = 1
        while index < len(self.seznam_oddilu):
            j = index - 1
            while j >= 0:
                if self.seznam_oddilu[j + 1].obsazenost() < self.seznam_oddilu[j].obsazenost():
                    temp = self.seznam_oddilu[j]
                    self.seznam_oddilu[j] = self.seznam_oddilu[j + 1]
                    self.seznam_oddilu[j + 1] = temp
                    j -= 1
                else:
                    break
            index += 1

    # spocita rozdil mezi oddilem s nejmensim prumernym vekem a nejvetsim prumernym vekem
    def vekovy_rozdil_min_max(self):
        min = 1000
        max = -1000
        for oddil in self.seznam_oddilu:
            average = oddil.avg_age()
            if average < min:
                min = oddil.avg_age()
            if average > max:
                max = average
        return abs(max-min)

    # TODO zaridit, abych daval deti do oddilu, ktere maji malo deti, takze ne nutne postupne, protoze prirazenim kamaradu vzniknou nerovnosti v tom kdo ma kolik deti
    def zaradit_deti_do_oddilu(self, seznam_deti, margin, aktualni_index_oddil=0, otocka=False, smer_tam=True, ):
        # seznam_deti.seznam_deti.sort(key=lambda x: x.pocet_kamaradu(), reverse=True) # todo ma tohle smysl?
        # aktualni_index_oddil = 0
        # otocka = False  # indikuje kdyz dosahneme konce seznamu a otacime smer prochazeni
        # smer_tam = True  # indikuje aktualni smer projizdeni seznamem
        while not len(seznam_deti.seznam_deti) == 0:
            oddil = self.seznam_oddilu[aktualni_index_oddil]  # aktualni oddil
            pridat_dite = True
            dite = seznam_deti.seznam_deti.pop(
                0)  # teoreticky pokud u posledniho oddilu v seznamu vybereme dite s mladsim kamaradem, tak pak tento oddil hned preskocime a
            # otestovat, jestli nema dite uz oddil
            while not dite.oddil == -1:
                if len(seznam_deti.seznam_deti) == 0:  # if we are at the end of a list hadle it
                    pridat_dite = False
                    break
                else:
                    dite = seznam_deti.seznam_deti.pop(0)
            if pridat_dite:
                if not add_kid_to_oddil(oddil, dite,
                                        margin):  # zaradi do oddilu dite a vsechny jeho kamarady a incrementne pripadne counter pauzirovani
                    seznam_deti.seznam_deti.insert(0, dite)  # pokud jsme nepridali dite do oddilu, vratime ho do seznamu
                    # ted musime vyzkouset dalsi dite, dokud nenajdeme dite, ktere sem pasuje


            # incrementujeme index prochazeni oddily a aktualizuje otocku a smer_tam
            aktualni_index_oddil, otocka, smer_tam = increment_index_tam_a_zpet(aktualni_index_oddil,
                                                                                len(self.seznam_oddilu), otocka,
                                                                                smer_tam)
        return otocka, smer_tam, aktualni_index_oddil

    def zaradit_deti_do_oddilu_tam_zpet(self, seznam_deti, margin, aktualni_index_oddil=0, otocka=False, smer_tam=True):
        # seznam_deti.seznam_deti.sort(key=lambda x: x.pocet_kamaradu(), reverse=True) # todo ma tohle smysl?
        # aktualni_index_oddil = 0
        # otocka = False  # indikuje kdyz dosahneme konce seznamu a otacime smer prochazeni
        # smer_tam = True  # indikuje aktualni smer projizdeni seznamem
        while not len(seznam_deti.seznam_deti) == 0:
            oddil = self.seznam_oddilu[aktualni_index_oddil]  # aktualni oddil
            pridat_dite = True
            dite = seznam_deti.seznam_deti.pop(
                0)  # teoreticky pokud u posledniho oddilu v seznamu vybereme dite s mladsim kamaradem, tak pak tento oddil hned preskocime a
            # otestovat, jestli nema dite uz oddil
            while not dite.oddil == -1:
                if len(seznam_deti.seznam_deti) == 0:  # if we are at the end of a list hadle it
                    pridat_dite = False
                    break
                else:
                    dite = seznam_deti.seznam_deti.pop(0)
            if pridat_dite:
                mam_pridat, duvod = add_kid_to_oddil(oddil, dite, margin)
                if not mam_pridat:  # pokud se dite nepovedlo pridat, zjistit jestli je duvod misto(1) nebo kvuli
                    # seznamu lidi navic(0)
                    if not duvod:
                        # pokud je to kvuli seznamu lidi navic, tak jdu na dalsi oddil a nezkousim dalsi deti
                        seznam_deti.seznam_deti.insert(0, dite)  # pokud jsme nepridali dite do oddilu, vratime ho do seznamu
                    # pokud to je kvuli mistu, tak jsme uz mame skoro zaplnene oddily a musime se podivat na oddil s
                    # nejvetsim vekovym prumere, vzit mu nejmladsi dite, ktere nema kamarada a zaradit tam dvpjci,
                    # trojci. jeste zkontrolovat, ze oddil neni uz plny
                    elif oddil.kapacita > 0:
                        temp_seznam_oddilu = self.seznam_oddilu.copy()
                        temp_seznam_oddilu.sort(key=lambda x: x.avg_age(), reverse=True)  # serad od nejstarsiho
                        # najdi oddil pro switch by melo vzdycky najit nejaky oddil, pokud ne, a nemam osetreny
                        # vsechny situace, tak bude problem
                        if dite.name == "Kopecká Tereza":
                            print(dite.name)
                        vhodny_oddil, uvolneno = najit_oddil_pro_switch(temp_seznam_oddilu, dite)
                        if vhodny_oddil is None:
                            print("oddil is  none")
                        # odstran z vhodneho oddilu pocet deti = uvolneno
                        while uvolneno > 0:
                            temp_dite = vhodny_oddil.seznam_deti[len(vhodny_oddil.seznam_deti) - uvolneno]
                            vhodny_oddil -= temp_dite
                            temp_dite.oddil = -1
                            seznam_deti.seznam_deti.append(temp_dite)  # pridat dite opet do seznamu deti k zarazeni
                            uvolneno -= 1
                        # po odstraneni deti muzeme pridat nasi dvojci, trocji
                        mam_pridat, duvod = add_kid_to_oddil(vhodny_oddil, dite, margin)  # fixme mozna dat margin +inf aby to nekdy nehodilo chybu?
                    else:  # kapacita oddilu je 0 a duvod nezarazeni je malo mista v oddile, vrat tedy dite z5 do seznamu
                        seznam_deti.seznam_deti.insert(0, dite)

            # incrementujeme index prochazeni oddily a aktualizuje otocku a smer_tam
           # aktualni_index_oddil, otocka, smer_tam = increment_index_tam_a_zpet(aktualni_index_oddil,
            #                                                                    len(self.seznam_oddilu), otocka,
             #                                                                   smer_tam)
            if aktualni_index_oddil + 1 == len(self.seznam_oddilu):
                aktualni_index_oddil = 0
            else:
                aktualni_index_oddil += 1
        return aktualni_index_oddil, otocka, smer_tam

    def zaradit_kluky_do_oddilu(self, seznam_deti, margin, pocet_kluku_na_oddil, aktualni_index_oddil=0):
        pridat_dite = True

        while not len(seznam_deti.seznam_deti) == 0:
            oddil = self.seznam_oddilu[aktualni_index_oddil]  # aktualni oddil

            # zjistit, jestli nemame prave dve nebo tri posledni mista, abychom pridali dvojci, trojci
            if len(pocet_kluku_na_oddil) == 0:
                assert neexistuje_dite_bez_oddilu(seznam_deti.seznam_deti)

            else:
                maximum_kluku = max(pocet_kluku_na_oddil)
                minimum_kluku = min(pocet_kluku_na_oddil)

            # skontrolovat, ze oddil neprekrocil kapacitu
            if oddil.pocet_kluku >= maximum_kluku:
                if neexistuje_dite_bez_oddilu(seznam_deti.seznam_deti):
                    break
                if aktualni_index_oddil + 1 == len(self.seznam_oddilu):
                    aktualni_index_oddil = 0
                else:
                    aktualni_index_oddil += 1
                continue

            get_twins = False
            get_triplet = False

            #TODO: tohle chceme odstranit, kdyz delame jen kluky. jen si nejak dat pozor, aby se rozrazeni povedlo,
            # protoze kapacita je taky omezena. Tohle to resi, alle zase tozpusobuje treba to, ze mam trojci starejch
            # deti cca kolem 15ti a pak jim pridam trojci nejakejch trinactiletejch
            if oddil.pocet_kluku == maximum_kluku - 3:
                get_triplet = True
            elif oddil.pocet_kluku == maximum_kluku - 2:
                get_twins = True
            if oddil.pocet_kluku == minimum_kluku - 3:
                get_triplet = True
            elif oddil.pocet_kluku == minimum_kluku - 2:
                get_twins = True


            # ted bud hledame dvojci nebo trojci, nebo nam je to jedno
            kid = None
            kid2 = None
            kid3 = None
            if get_triplet:
                kid3 = find_triplet(seznam_deti.seznam_deti, oddil, margin)
            if get_twins:
                kid2 = find_twins(seznam_deti.seznam_deti, oddil, margin)
            if kid2 is None and kid3 is None: # tady ted musime najit dite bez kamarada
                kid = seznam_deti.seznam_deti.pop(
                    0)  # teoreticky pokud u posledniho oddilu v seznamu vybereme dite s mladsim kamaradem, tak pak tento oddil hned preskocime a
                # otestovat, jestli nema dite uz oddil
                while not kid.oddil == -1:
                    if len(seznam_deti.seznam_deti) == 0:  # if we are at the end of a list hadle it
                        pridat_dite = False
                        break
                    else:
                        kid = seznam_deti.seznam_deti.pop(0)

            if pridat_dite:
                if kid3 is not None:
                    add_kid_to_oddil(oddil, kid3, margin, maximum_kluku)
                    if kid2 is not None:
                        seznam_deti.seznam_deti.insert(0, kid2)
                        seznam_deti.seznam_deti.sort(key=lambda x: x.age, reverse=True)
                elif kid2 is not None:
                    add_kid_to_oddil(oddil, kid2, margin, maximum_kluku)
                    if kid3 is not None:
                        seznam_deti.seznam_deti.insert(0, kid3)
                        seznam_deti.seznam_deti.sort(key=lambda x: x.age, reverse=True)
                #TODO: zajistit, aby se v mam pridat funkci kdyz delam kluky nestalo to, ze mam dvojci kluk holka, ale
                # aplikuju na ne maximum kluku a kvuli tomu je nepridam
                else:
                    uspech, duvod = add_kid_to_oddil(oddil, kid, margin, maximum_kluku)
                    if not uspech:
                        seznam_deti.seznam_deti.insert(0, kid)

                if oddil.pocet_kluku == maximum_kluku:
                    pocet_kluku_na_oddil.remove(maximum_kluku)
            if aktualni_index_oddil + 1 == len(self.seznam_oddilu):
                aktualni_index_oddil = 0
            else:
                aktualni_index_oddil += 1

        return self

    def naplnit_oddily_na_kapitana_varianta(self, seznam_deti, margin):
        avg_age = seznam_deti.prumerny_vek()

        # oddelit kluky a holky
        seznam_g = seznam_deti.seznam_holek()
        seznam_b = seznam_deti.seznam_kluku()
        # seradit od nejstarsiho po nejmladsi
        seznam_g.seradit_sestupne()
        seznam_b.seradit_sestupne()
        # spocitej kolik kluku muze mit maximalne jeden oddil
        pocet_kluku_na_oddil = vytvor_pocty_kluku_na_oddil(len(seznam_b.seznam_deti), len(self.seznam_oddilu))
        # print(seznam_g)
        # print(seznam_b)
        # nejdriv zarad kluky
        self.zaradit_kluky_do_oddilu(seznam_b, margin, pocet_kluku_na_oddil)
        # seradit oddily podle nejnizsiho veku po nejvyssi
        # self.seznam_oddilu.sort(key=lambda x: x.avg_age())
        self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu, reverse=True)
        # print(self)
        # zarad holky
        self.zaradit_deti_do_oddilu_tam_zpet(seznam_g, margin)

        # tweak treba radov 4 tatari, mam 60 kluku a 10 oddilu, chtel bych 6 do kazdeho, ted ale dva oddily maji 7 a dva 5
        #  protoze v situaci ze uz je v oddile 5 kluku pridavame dvojci mladych kluku takze jich je sedm. teoreticky by se
        #  mohlo stat, ze pridame i trojci, pak by to byl jeste vetsi problem. Resenim by bylo nejdrive rozradit vsechny
        #  dvojice a trojce a potom zbytek,

    def naplnit_oddily_na_kapitana(self, seznam_deti, margin):
        avg_age = seznam_deti.prumerny_vek()

        # oddelit kluky a holky
        seznam_g = seznam_deti.seznam_holek()
        seznam_b = seznam_deti.seznam_kluku()
        # seradit od nejstarsiho po nejmladsi
        seznam_g.seradit_sestupne()
        seznam_b.seradit_sestupne()
        # vytvrit seznamy kde jsou jen lidi s kamaradem a ty rozradit nejdriv
        seznam_b_kamaradi = seznam_b.vyselektovat_deti_s_kamaradem()
        seznam_g_kamaradi = seznam_g.vyselektovat_deti_s_kamaradem()
        # seradit od nejstarsiho
        # inicializace promene ktera bude indexem oddilu
        # dokud existuje dite bez oddilu prirazuj jako kdyz jsou kapitani, zacni klukama
        # nejdriv zarad kluky co maj kamose
        # print(seznam_b_kamaradi)
        self.zaradit_deti_do_oddilu_tam_zpet(seznam_b_kamaradi, margin)

        # seradit oddily podle nejnizsiho veku po nejvyssi
        # a pak seradit podle nejmin lidi po nejvic lidi
        self.seznam_oddilu.sort(key=lambda x: x.avg_age())
        # print(self)
        # self.seznam_oddilu.sort(key=lambda x: x.kapacita, reverse=True)
        # zarad holky co maj kamosky
        # self.zaradit_deti_do_oddilu(seznam_g_kamaradi, margin)
        # self.seznam_oddilu.sort(key=lambda x: x.avg_age())
        # print(self)
        # self.seznam_oddilu.sort(key=lambda x: x.kapacita, reverse=True)
        # zarad zbytek kluku
        self.zaradit_deti_do_oddilu_tam_zpet(seznam_b, margin)
        print(self)
        self.seznam_oddilu.sort(key=lambda x: x.avg_age())
        # self.seznam_oddilu.sort(key=lambda x: x.kapacita, reverse=True)
        # zarad zbytek holek
        self.zaradit_deti_do_oddilu_tam_zpet(seznam_g, margin)  # aktualni_index_oddil, otocka, smer_tam

    def naplnit_oddily(self, seznam_deti):
        # prijde seznam deti uz serazenej?
        counter = 0
        avg_age = seznam_deti.prumerny_vek()

        # oddelit kluky a holky
        seznam_g = seznam_deti.seznam_holek()
        seznam_b = seznam_deti.seznam_kluku()
        # prumerny vek kluku a holek zvlast
        avg_age_b = seznam_b.prumerny_vek()
        avg_age_g = seznam_g.prumerny_vek()
        print("avg_age is ", avg_age, " avg age boyz is ", avg_age_b, " avg age girls is ", avg_age_g)

        # nejdriv naplnit oddily klukama, pak holkama a cilit na prumerny vek
        # kluci
        # projet oddil po oddile a vzdy priradit jednoho pionyra, aby byl co nejblize prumeru
        while seznam_b.nezarazene_deti():  # asi by bylo dobry udelat z toho funkci
            for oddil in self.seznam_oddilu:
                # check if oddil has to pause
                if not oddil.pauzirovani == 0:
                    oddil.pauzirovani -= 1
                    continue
                min_odchylka = 100
                best_kid = None
                # priradit dite
                for dite in seznam_b.seznam_deti:
                    if dite.oddil == -1:
                        oddil += dite  # pridat dite do seznamu
                        temp_avg_age = oddil.avg_age()  # spocitat vekovy prumer a ulozit pokud je nejbliz k prumeru
                        odchylka = abs(temp_avg_age - avg_age_b)
                        if odchylka < min_odchylka:
                            min_odchylka = odchylka
                            best_kid = dite  # ulozit nejlepsi dite
                        oddil -= dite  # ostranit dite a zkusit dalsi
                if best_kid is not None:
                    # ulozit nejlepsi dite a vsechny jeho/jeji kamose
                    oddil += best_kid
                    best_kid.oddil = oddil
                    if not best_kid.kamarad1 == -1:
                        oddil += best_kid.kamarad1
                        best_kid.kamarad1.oddil = oddil
                        oddil.pauzirovani += 1
                    if not best_kid.kamarad2 == -1:
                        oddil += best_kid.kamarad2
                        best_kid.kamarad2.oddil = oddil
                        oddil.pauzirovani += 1

        for oddil in self.seznam_oddilu:
            print(oddil)
        self.seradit_oddily_vzestupne()  # abychom zacali pridavat deti do oddilu kde je nejmene deti

        # zaradit holky
        while seznam_g.nezarazene_deti():  # asi by bylo dobry udelat z toho funkci
            for oddil in self.seznam_oddilu:
                if not oddil.pauzirovani == 0:
                    oddil.pauzirovani -= 1
                    continue
                min_odchylka = 100
                best_kid = None
                # priradit dite
                for dite in seznam_g.seznam_deti:
                    if dite.oddil == -1:
                        oddil += dite
                        temp_avg_age = oddil.avg_age()  # spocitat vekovy prumer a ulozit pokud je nejbliz k prumeru
                        diff_avg_age = temp_avg_age - avg_age
                        odchylka = abs(avg_age - diff_avg_age)
                        if odchylka < min_odchylka:
                            min_odchylka = odchylka
                            best_kid = dite  # ulozit nejlepsi dite
                        oddil -= dite  # ostranit dite a zkusit dalsi
                if best_kid is not None:
                    oddil += best_kid
                    best_kid.oddil = oddil
                    if not best_kid.kamarad1 == -1:
                        oddil += best_kid.kamarad1
                        best_kid.kamarad1.oddil = oddil
                        oddil.pauzirovani += 1
                    if not best_kid.kamarad2 == -1:
                        oddil += best_kid.kamarad2
                        best_kid.kamarad2.oddil = oddil
                        oddil.pauzirovani += 1

        return self
    # 66


class Oddil:

    def __init__(self, cislo_oddilu = -1, kapacita=15):
        self.kapacita = kapacita
        self.pocet_kluku = 0
        self.seznam_deti = []
        self.pocet_chatek = 0
        self.seznam_cisel_chatek = []
        self.cislo_oddilu = cislo_oddilu
        self.pauzirovani = 0  # kdyz si oddil vybere vic jak jedno dite, pricte se mu sem 1 nebo dva aby priste pauziroval
        self.pauzirovani_b = 0  # separatni counter pro kluky a holky
        self.pauzirovani_g = 0
        self.dalsi_narade = True  # pri rozrazovani urcuje jestli se dalsi priradi kluk nebo holka. Kluk = true, holka = false
        self.seznam_lidi_navic = []
        self.kapacita_kluku = 7
        # holek T54,c48
        # kluku T66, C48

    def __iadd__(self, other):
        assert isinstance(other, Dite)
        if other.gender == "H":
            self.pocet_kluku += 1
        self.seznam_deti.append(other)
        self.kapacita -= 1
        return self

    def __isub__(self, other):
        assert isinstance(other, Dite)
        self.seznam_deti.remove(other)
        self.kapacita += 1
        return self

    def __str__(self):
        result = 'Oddil c. {:d} with capacity {:d} including {:d} boys with average age of {:f} je v {:d} chatkach: \n'.format(
            self.cislo_oddilu, self.kapacita, self.pocet_kluku, self.avg_age(), self.pocet_chatek)
        # spocitat, v kolika chatkach mame ubytovany deti

        for element in self.seznam_deti:
            result += str(element)
        result += "\n"
        return result

    def urcit_pocet_chatek(self):
        for dite in self.seznam_deti:
            if dite.chatka is not None and self.seznam_cisel_chatek.count(dite.chatka.cislo_chatky) == 0:
                self.seznam_cisel_chatek.append(dite.chatka.cislo_chatky)
                self.pocet_chatek += 1
        return self


    def seradit_sestupne_deti(self):
        index = 1
        while index < len(self.seznam_deti):
            j = index - 1
            while j >= 0:
                if self.seznam_deti[j + 1].age > self.seznam_deti[j].age:
                    temp = self.seznam_deti[j]
                    self.seznam_deti[j] = self.seznam_deti[j + 1]
                    self.seznam_deti[j + 1] = temp
                    j -= 1
                else:
                    break
            index += 1
        return self

    def obsazenost(self):
        return len(self.seznam_deti)


    def delete(self, dite, valueError="None"):
        assert isinstance(dite, Dite)
        try:
            self.seznam_deti.remove(dite)
        except valueError as v:
            print('tohle dite v seznamu vazne neni!! ', v)
        return self

    def avg_age(self):
        summ = 0
        count = 0
        for element in self.seznam_deti:
            summ += element.age
            count += 1
        if count == 0:
            return -1
        else:
            return summ / count
