package beat;


public class LinkedList {
    public int index;
    public float value;
    public LinkedList next;

    public LinkedList (int index){
        this.index = index;
    }
    public LinkedList (int index, float value, LinkedList next){
        this.index = index;
        this.value = value;
        this.next = next;
    }
    public  LinkedList (){
        this.value = 0;
        this.index = 0;
        this.next = null;
    }
    public static LinkedList addIndexToList (int index, LinkedList l){
        if (l == null){
            return new LinkedList(index);
        }
        else {
            l.next = addIndexToList(index, l.next);
            return l;
        }

    }

    public static LinkedList listOfIndexes (long pB, int index){
        if (index > 63) return null;
        if ((pB>>>index & 1) == 1){
            LinkedList list = new LinkedList(index, BitBeat.bonusPoints[index], null);
            list.next = listOfIndexes(pB, ++index);
            return list;
        }
        else {
            return listOfIndexes(pB, ++index);
        }
    }
    public static int numberOfNodes (LinkedList l){
        if (l == null) return 0;
        return numberOfNodes(l.next) + 1;
    }

}
