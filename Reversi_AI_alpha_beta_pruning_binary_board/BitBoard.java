package beat;

import reversi.Coordinates;
import reversi.GameBoard;
import reversi.OutOfBoundsException;

public class BitBoard {
    private long pB; //players board
    private long oB; //opponents board
    private long eB; //keeps track of empty fields
    private final long one;
    private final long leftShift = -217020518514230020L;
    private final long rightShift = 4557430888798830399L;
    private final long leftEdgeDelete = -72340172838076674L;
    private final long rightEdgeDelete = 9187201950435737471L;
    private final long topAndBotEdge = -72057594037927681L;
    private final long leftAndRightEdge = -9114861777597660799L;
    private final long allEdgesDelete = 35604928818740736L;

    public BitBoard() {
        pB = 0L;
        oB = 0L;
        eB = 0L;
        one = 1L;
    }

    public BitBoard(long pB, long oB, long eB) {
        this.pB = pB;
        this.oB = oB;
        this.eB = eB;
        this.one = 1L;
    }

    public long geteB() {
        return eB;
    }

    public long getoB() {
        return oB;
    }

    public long getpB() {
        return pB;
    }

    public BitBoard cloneBoard() {
        return new BitBoard(pB, oB, eB);
    }

    public boolean isFull() {
        return (eB == 0);
    }

    public int stonesPlayed() {
        return (Long.bitCount(pB) + Long.bitCount(oB));
    }

    public int countStones(int player) {
        if (player == BitBeat.player) {
            return Long.bitCount(pB);
        } else {
            return Long.bitCount(oB);
        }
    }

    public float stabilityBoardEval() {
        return (staticStability(pB) + Long.bitCount(viableMoves(BitBeat.player)) + stabilityInsideBoard(pB, oB)) - (staticStability(oB) + Long.bitCount(viableMoves(BitBeat.opponent)) + stabilityInsideBoard(oB, pB));

    }

    private float staticStability(long pB) {
        float stability = 0;
        int[] positionIndexes = BitBeat.listOfMoves(pB, Long.bitCount(pB));
        for (int i = 0; i < positionIndexes.length; i++) {
            stability += BitBeat.staticEvaluationPoints[positionIndexes[i]];
        }
        return stability;
    }

    private int stabilityTopBot(long pB, long oB) {
        long pTopBotBoard = pB & topAndBotEdge; //players top and bot board only
        long oTopBotBoard = oB & topAndBotEdge; //opponents top and bot board only
        long stonesTurned;
        int myStones = Long.bitCount(pTopBotBoard); //keeps track of how many of my stones survive

        //danger from the left
        stonesTurned = moveLeft(rightSearch(oTopBotBoard, pTopBotBoard), oTopBotBoard, pTopBotBoard);

        //danger from the right
        stonesTurned |= moveRight(leftSearch(oTopBotBoard, pTopBotBoard), oTopBotBoard, pTopBotBoard);
        if (myStones != 0) {
            return (myStones - Long.bitCount(stonesTurned)) * 4 / myStones;
        } else return 0;

    }

    private int stabilityLeftRight(long pB, long oB) {
        long pLeftRightBoard = pB & leftAndRightEdge;
        long oLeftRightBoard = oB & leftAndRightEdge;
        long stonesTurned;
        int myStones = Long.bitCount(pLeftRightBoard);

        //danger from the top
        stonesTurned = moveTop(bottomSearch(oLeftRightBoard, pLeftRightBoard), oLeftRightBoard, pLeftRightBoard);

        //danger from the bottom
        stonesTurned |= moveDown(topSearch(oLeftRightBoard, pLeftRightBoard), oLeftRightBoard, pLeftRightBoard);

        if (myStones != 0) {
            return (myStones - Long.bitCount(stonesTurned)) * 4 / myStones;
        } else return 0;

    }

    private float stabilityInsideBoard(long pB, long oB) {
        long pInBoard = pB & allEdgesDelete;
        long oInBoard = oB & allEdgesDelete;
        long stonesTurned = 0L;

        stonesTurned |= moveDown(topSearch(oInBoard, pInBoard), oInBoard, pInBoard);

        stonesTurned |= moveLeftDown(topRightSearch(oInBoard, pInBoard), oInBoard, pInBoard);

        stonesTurned |= moveLeft(rightSearch(oInBoard, pInBoard), oInBoard, pInBoard);

        stonesTurned |= moveTopLeft(bottomRightSearch(oInBoard, pInBoard), oInBoard, pInBoard);

        stonesTurned |= moveTop(bottomSearch(oInBoard, pInBoard), oInBoard, pInBoard);

        stonesTurned |= moveTopRight(bottomLeftSearch(oInBoard, pInBoard), oInBoard, pInBoard);

        stonesTurned |= moveRight(leftSearch(oInBoard, pInBoard), oInBoard, pInBoard);

        stonesTurned |= moveRightDown(topLeftSearch(oInBoard, pInBoard), oInBoard, pInBoard);

        pInBoard ^= stonesTurned;

        return Long.bitCount(pInBoard) / 4; //trying to make the stable stones inside of the board more valuable as they are hard to get
        //todo: how to calculate the resulting stability??
    }

    public void makeMove(int player, long aMove) {
        if (player == BitBeat.player) {
            long result = (moveLeft(aMove, pB, oB) | moveTopLeft(aMove, pB, oB) | moveTop(aMove, pB, oB) | moveTopRight(aMove, pB, oB) | moveRight(aMove, pB, oB) | moveRightDown(aMove, pB, oB) | moveDown(aMove, pB, oB) | moveLeftDown(aMove, pB, oB));
            this.pB |= (aMove | result);
            this.oB = this.oB ^ result;
            this.eB ^= aMove;
        } else {
            long result = (moveLeft(aMove, oB, pB) | moveTopLeft(aMove, oB, pB) | moveTop(aMove, oB, pB) | moveTopRight(aMove, oB, pB) | moveRight(aMove, oB, pB) | moveRightDown(aMove, oB, pB) | moveDown(aMove, oB, pB) | moveLeftDown(aMove, oB, pB));
            this.pB ^= result;
            this.oB |= (aMove | result);
            this.eB = this.eB ^ aMove; //^keeps disappearing
        }
    }

    public long viableMoves(int player) {
        if (player == BitBeat.player) {
            return topLeftSearch(pB, oB) | topSearch(pB, oB) | topRightSearch(pB, oB) | rightSearch(pB, oB) | bottomRightSearch(pB, oB) | bottomSearch(pB, oB) | bottomLeftSearch(pB, oB) | leftSearch(pB, oB);
        } else {
            return topLeftSearch(oB, pB) | topSearch(oB, pB) | topRightSearch(oB, pB) | rightSearch(oB, pB) | bottomRightSearch(oB, pB) | bottomSearch(oB, pB) | bottomLeftSearch(oB, pB) | leftSearch(oB, pB);
        }

    }

    public void updateBoard(GameBoard g) {
        int position;
        long one = 1L;

        for (int index = 0; index < 64; index++) {
            Coordinates c = new Coordinates(index / 8 + 1, index % 8 + 1);
            try {
                position = g.getOccupation(c);
                if (position == BitBeat.player) {
                    pB |= one << index;
                } else if (position == BitBeat.opponent) {
                    oB |= one << index;
                } else {
                    eB |= one << index;
                }
            } catch (OutOfBoundsException e) {
                System.out.println("wrong coordinates mate :)");
            }

        }
    }

    private long topSearch(long pB, long oB) {
        long candidates = (pB >>> 8) & oB;
        long result = 0L;
        while (candidates != 0) {
            result |= (candidates >>> 8) & eB;
            candidates = candidates >>> 8 & oB;
        }
        return result;
    }

    private long bottomSearch(long pB, long oB) {
        long candidates = pB << 8 & oB;
        long result = 0L;
        while (candidates != 0) {
            result |= candidates << 8 & eB;
            candidates = candidates << 8 & oB;
        }
        return result;
    }

    private long leftSearch(long pB, long oB) { //todo: add if condition before we even star initializing everything if(pB<<1 & oB != 0)
        pB &= leftShift;
        oB &= rightEdgeDelete;
        long candidates = pB >>> 1 & oB;
        long eBtemp = eB;
        eBtemp &= rightEdgeDelete;
        long result = 0L;
        while (candidates != 0) {
            result |= candidates >>> 1 & eBtemp;
            candidates = candidates >>> 1 & oB;
        }
        return result;

    }

    private long rightSearch(long pB, long oB) { //todo: add if condition before we even star initializing everything if(pB<<1 & oB != 0)
        pB &= rightShift;
        oB &= leftEdgeDelete;
        long candidates = pB << 1 & oB;
        long eBtemp = eB;
        eBtemp &= leftEdgeDelete;
        long result = 0L;
        while (candidates != 0) {
            result |= candidates << 1 & eBtemp;
            candidates = candidates << 1 & oB;
        }
        return result;
    }

    private long topLeftSearch(long pB, long oB) {
        long eBtemp = eB;
        pB &= leftShift;
        oB &= rightEdgeDelete;
        eBtemp &= rightEdgeDelete;
        long candidates = pB >>> 9 & oB;
        long result = 0L;
        while (candidates != 0) {
            result |= candidates >>> 9 & eBtemp;
            candidates = candidates >>> 9 & oB;
        }
        return result;
    }

    private long topRightSearch(long pB, long oB) {
        long eBtemp = eB;
        pB &= rightShift;
        oB &= leftEdgeDelete;
        eBtemp &= leftEdgeDelete;
        long candidates = pB >>> 7 & oB;
        long result = 0L;
        while (candidates != 0) {
            result |= candidates >> 7 & eBtemp;
            candidates = candidates >>> 7 & oB;
        }
        return result;
    }

    private long bottomLeftSearch(long pB, long oB) {
        long eBtemp = eB;
        pB &= leftShift;
        oB &= rightEdgeDelete;
        eBtemp &= rightEdgeDelete;
        long candidates = pB << 7 & oB;
        long result = 0L;
        while (candidates != 0) {
            result |= candidates << 7 & eBtemp;
            candidates = candidates << 7 & oB;
        }
        return result;
    }

    private long bottomRightSearch(long pB, long oB) {
        long eBtemp = eB;
        pB &= rightShift;
        oB &= leftEdgeDelete;
        eBtemp &= leftEdgeDelete;
        long candidates = pB << 9 & oB;
        long result = 0L;
        while (candidates != 0) {
            result |= candidates << 9 & eBtemp;
            candidates = candidates << 9 & oB;
        }
        return result;
    }

    public long moveTop(long aMove, long pB, long oB) {
        long candidate = aMove >>> 8 & oB;
        // index -= 8;
        long result = 0L;
        while (candidate != 0) {
            result |= candidate;
            if ((candidate >>> 8 & pB) != 0) {
                return result;
            } else {
                //  index -= 8;
                candidate = candidate >>> 8 & oB;
            }
        }
        return 0L;
    }

    public long moveTopLeft(long aMove, long pB, long oB) {
        pB &= rightEdgeDelete;
        oB &= rightEdgeDelete;
        long candidate = aMove >>> 9 & oB;
        // index -= 9;
        long result = 0L;
        while (candidate != 0) {
            result |= candidate;
            if ((candidate >>> 9 & pB) != 0) {
                return result;
            } else {
                // index -= 9;
                candidate = candidate >>> 9 & oB;
            }
        }
        return 0L;
    }

    public long moveTopRight(long aMove, long pB, long oB) {
        pB &= leftEdgeDelete;
        oB &= leftEdgeDelete;
        long candidate = aMove >>> 7 & oB;
        // index -= 7;
        long result = 0L;
        while (candidate != 0) {
            result |= candidate;
            if ((candidate >>> 7 & pB) != 0) {
                return result;
            } else {
                // index -= 7;
                candidate = candidate >>> 7 & oB;
            }
        }
        return 0L;
    }

    public long moveLeft(long aMove, long pB, long oB) {
        pB &= rightEdgeDelete;
        oB &= rightEdgeDelete;
        long candidate = aMove >>> 1 & oB;
        //index -= 1;
        long result = 0L;
        while (candidate != 0) {
            result |= candidate;
            if ((candidate >>> 1 & pB) != 0) {
                return result;
            } else {
                // index -= 1;
                candidate = candidate >>> 1 & oB;
            }
        }
        return 0L;
    }

    public long moveRight(long aMove, long pB, long oB) {
        pB &= leftEdgeDelete;
        oB &= leftEdgeDelete;
        long candidate = aMove << 1 & oB;
        //index += 1;
        long result = 0L;
        while (candidate != 0) {
            result |= candidate;
            if ((candidate << 1 & pB) != 0) {
                return result;
            } else {
                //  index += 1;
                candidate = candidate << 1 & oB;
            }
        }
        return 0L;
    }

    public long moveRightDown(long aMove, long pB, long oB) {
        pB &= leftEdgeDelete;
        oB &= leftEdgeDelete;
        long candidate = aMove << 9 & oB;
        //index += 9;
        long result = 0L;
        while (candidate != 0) {
            result |= candidate;
            if ((candidate << 9 & pB) != 0) {
                return result;
            } else {
                //index += 9;
                candidate = candidate << 9 & oB;
            }
        }
        return 0L;
    }

    public long moveDown(long aMove, long pB, long oB) {
        long candidate = aMove << 8 & oB;
        // index += 8;
        long result = 0L;
        while (candidate != 0) {
            result |= candidate;
            if ((candidate << 8 & pB) != 0) {
                return result;
            } else {
                //  index += 8;
                candidate = candidate << 8 & oB;
            }
        }
        return 0L;
    }

    public long moveLeftDown(long aMove, long pB, long oB) {
        pB &= rightEdgeDelete;
        oB &= rightEdgeDelete;
        long candidate = aMove << 7 & oB;
        // index += 7;
        long result = 0L;
        while (candidate != 0) {
            result |= candidate;
            if ((candidate << 7 & pB) != 0) {
                return result;
            } else {
                //  index += 7;
                candidate = candidate << 7 & oB;
            }
        }
        return 0L;
    }
}
