
#vezme 2D numpy seznam a vytvori z nej seznam deti
#def inicializace_seznamu_deti (vstup, offset):
#    input_v = vypocitat_vek(vstup, offset) #vytvoreny seznam deti
#    seznam = Seznam_deti()
#    seznam.seznam_deti = input_v

#    return seznam


def _rating_list(rating_list):
    index = 1
    while index < len(rating_list):
        j = index - 1
        j = index - 1
        while j >= 0:
            if rating_list[j + 1][0] > rating_list[j][0]:
                temp = rating_list[j][0]
                rating_list[j][0] = rating_list[j + 1][0]
                rating_list[j + 1][0] = temp
                j -= 1
            else:
                break
        index += 1


def kapacity_z_rozlozeni(rozlozeni):
    result = [[0 for x in range(4)] for y in range(10)] #4 sloupce a maximum 10 typu chatek
    for index in range(len(rozlozeni)):
        kapacita = int(rozlozeni[index])
        for row, element in enumerate(result):
            if element[0] == kapacita:  # we found the same input, add one
                element[1] += 1
                break
            elif element[0] == 0:  # we didnt find same chatka, add it in the first free line
                element[0] = kapacita
                element[1] += 1
                break  # and stop the loop
    return result




# seznam deti je trida senam deti, ktere maji dle dostupnych kapacit rozdeleny do chatek
# pohlavi True = holky , False = kluci,
def jde_rozdelit(pocet_deti, zasobnik_kapacit, rozlozeni, maly_holky):
    if pocet_deti == 0:  # recursion end
        return True, rozlozeni

    for index, element in enumerate(zasobnik_kapacit):
        kapacita = element[0]
        mnozstvi = element[1]
        age_restriction = int(element[2])
        if kapacita == 0:  # dosahli jsme konce seznamu
            break
        if maly_holky and pocet_deti >= kapacita and mnozstvi > 0 and age_restriction > 0:
            element[1] -= 1  # snizi pocet chatek s dannou kapacitou
            rozlozeni += str(kapacita)  # ulozi pridanou kapacitu do stringu
            result, rozlozeni = jde_rozdelit(pocet_deti - kapacita, zasobnik_kapacit, rozlozeni, maly_holky)
            if result:
                return result, rozlozeni
            else:
                print(("neco se pokazilo, rozrazujeme maly holky, to by se melo povyst, ale nepovedlo :("))

        elif not maly_holky and pocet_deti >= kapacita and mnozstvi > 0:  # kdybych chtel omezit kapacitu na jeden seznam, muzu to udelat jednoduse tady
            element[1] -= 1  # snizi pocet chatek s dannou kapacitou
            rozlozeni += str(kapacita)  # ulozi pridanou kapacitu do stringu
            result, rozlozeni = jde_rozdelit(pocet_deti - kapacita, zasobnik_kapacit, rozlozeni, maly_holky)
            if result:
                return result, rozlozeni
            else:
                element[1] += 1
                rozlozeni = rozlozeni[:len(rozlozeni)-1] #odebere posledni vstup
    # rozlozeni += "-"  #znak abych poznal ze zacina zbyte
    # rozlozeni += str(pocet_deti) #nepovedlo se najit presne ozlozeni, vrat zbyte
    return False, rozlozeni


# seznam_deti je serazen sestupne podle veku
# sixteen je bool jestli mame seznam 16+ nebo ne, podseznam je list, seznam_deti je trida Seznam_deti
def pridat_jedno_do_seznamu(podseznam, seznam_deti, sixteen):
    for index, dite in enumerate(seznam_deti.seznam_deti):
        if sixteen:
            if dite.kamarad1 == -1:
                podseznam.append(seznam_deti.seznam_deti.pop(index))
                return
        else:
            kamarad1 = dite.kamarad1
            kamarad2 = dite.kamarad2
            podseznam.append(seznam_deti.seznam_deti.pop(index))
            if not kamarad1 == -1 and dite.gender == kamarad1.gender:
                index2 = seznam_deti.seznam_deti.index(kamarad1) #najdu kde se kamarad nachazi
                podseznam.append(seznam_deti.seznam_deti.pop(index2))
            if not kamarad2 == -1 and dite.gender == kamarad2.gender:
                index2 = seznam_deti.seznam_deti.index(kamarad2)  # najdu kde se kamarad nachazi
                podseznam.append(seznam_deti.seznam_deti.pop(index2))
            return

#skontroluje jestli dite nema ekvivalentni protejsek v seznamu lidi navic, pokud ano, odstrani ekvivalentni dite
# ze seznamu
def mam_pridat_dite(oddil, dite, margin, maximum_kluku=-1): # TODO zajistit aby se vsechni kamaradi ze seznamu lidi navic nahradili nekym jinym
    # zkontrolovat, jestli je v oddile jeste dost mista pro dite a jeho kamarady
    # potrebuju return information whether its false cause of space or because there is a kid in the seznam_lidi_navic list
    pocet = 1
    pocet_kluku = 0
    kamarad1 = dite.kamarad1
    kamarad2 = dite.kamarad2
    if not kamarad1 == -1:
        pocet += 1
    if not kamarad2 == -1:
        pocet += 1
    #otestovat, jestli nebyl prekrocen limit u kluku
    if maximum_kluku != -1 and oddil.pocet_kluku + pocet > maximum_kluku:
        return False, 1
    if oddil.kapacita < pocet:
        return False, 1  # 1 kdyz je mala kapacita a 0 kdyz je dite v seznamu_lidi_navic
    for index, tabornik in enumerate (oddil.seznam_lidi_navic):
        if (abs(dite.age - tabornik.age) <= margin or dite.age < tabornik.age) and dite.gender == tabornik.gender:
            oddil.seznam_lidi_navic.pop(index) # odstran ekvivalent a vrat false
            return False, 0
        # to same skontrolovat u kamaradu ditete, pokud maji ekvivalent v seznamu lidi navic tak nepridavat
        # tweak je otazka jestli kdyz to udelam i u kamaradu, tak treba nenajdu zadnej oddil kam toho cloveka s kamaradem muzu zaradit
        #  pak by se musel snizit margin
        if not kamarad1 == -1 and (abs(kamarad1.age - tabornik.age) <= margin or kamarad1.age < tabornik.age) and kamarad1.gender == tabornik.gender:
            oddil.seznam_lidi_navic.pop(index) # odstran ekvivalent a vrat false
            return False, 0
        if not kamarad2 == -1 and (abs(kamarad2.age - tabornik.age) <= margin or kamarad2.age < tabornik.age) and kamarad2.gender == tabornik.gender:
            oddil.seznam_lidi_navic.pop(index) # odstran ekvivalent a vrat false
            return False, 0
    return True, 2

def mam_pridat_dite_trojce_dvojce(oddil, dite, margin, maximum_kluku=-1):
    # zkontrolovat, jestli je v oddile jeste dost mista pro dite a jeho kamarady
    # potrebuju return information whether its false cause of space or because there is a kid in the seznam_lidi_navic list
    pocet = 1
    pocet_kluku = 0
    kamarad1 = dite.kamarad1
    kamarad2 = dite.kamarad2
    if not kamarad1 == -1:
        pocet += 1
    if not kamarad2 == -1:
        pocet += 1
    # otestovat, jestli nebyl prekrocen limit u kluku
    if maximum_kluku != -1 and oddil.pocet_kluku + pocet > maximum_kluku:
        return False, 1
    if oddil.kapacita < pocet:
        return False, 1  # 1 kdyz je mala kapacita a 0 kdyz je dite v seznamu_lidi_navic
    for index, tabornik in enumerate(oddil.seznam_lidi_navic):
        if (abs(dite.age - tabornik.age) <= margin or dite.age < tabornik.age) and dite.gender == tabornik.gender:
             # neodstran ekvivalent a vrat false
            return False, 0
        # to same skontrolovat u kamaradu ditete, pokud maji ekvivalent v seznamu lidi navic tak nepridavat
        # tweak je otazka jestli kdyz to udelam i u kamaradu, tak treba nenajdu zadnej oddil kam toho cloveka s kamaradem muzu zaradit
        #  pak by se musel snizit margin
        if not kamarad1 == -1 and (abs(
                kamarad1.age - tabornik.age) <= margin or kamarad1.age < tabornik.age) and kamarad1.gender == tabornik.gender:
            # neodstran ekvivalent a vrat false
            return False, 0
        if not kamarad2 == -1 and (abs(
                kamarad2.age - tabornik.age) <= margin or kamarad2.age < tabornik.age) and kamarad2.gender == tabornik.gender:
            # neodstran ekvivalent a vrat false
            return False, 0
    return True, 2

def najit_oddil_pro_switch(seznam_oddilu, dite):
    for index, oddil in enumerate(seznam_oddilu):
        vhodny_oddil = True  # indikator, jestli se oddil bude hodit na switch
        # zjistit kolik potrebuju volnych mist
        uvolnit_mist = dite.pocet_kamaradu() + 1 - oddil.kapacita
        uvolneno = 0
        index_konce = len(oddil.seznam_deti) - 1
        while uvolnit_mist > 0:
            temp_dite = oddil.seznam_deti[index_konce]
            # pokud ma dite kamarada, nebrat a jit na dalsi oddil
            if not temp_dite.pocet_kamaradu() == 0:
                vhodny_oddil = False
                break
            else:
                uvolneno += 1
                index_konce -= 1
                uvolnit_mist -= 1
        if vhodny_oddil:
            return oddil, uvolneno

    print("najit_oddil_pro_switch nenaslo vhodny oddil. toto je vzacna a neosetrena situace, jmeno ditete je " + str(dite))
    return None, 0

def vytvor_pocty_kluku_na_oddil(pocet_kluku, pocet_oddilu):
    seznam = []
    min_pocet = pocet_kluku//pocet_oddilu
    pocet_oddilu_s_klukem_navic = pocet_kluku % pocet_oddilu
    while pocet_oddilu_s_klukem_navic > 0:
        seznam.append(min_pocet + 1)
        pocet_oddilu_s_klukem_navic -= 1
        pocet_oddilu -= 1

    while pocet_oddilu > 0:
        seznam.append(min_pocet)
        pocet_oddilu -= 1

    return seznam



def add_kid_to_oddil(oddil, dite, margin, maximum_kluku = -1):
    #skontrolovat jestli neni ekvivalentni dite v seznamu lidi navic, pokud ano, nepridavat,
    mam_pridat, duvod = mam_pridat_dite(oddil, dite, margin, maximum_kluku)
    if mam_pridat:
        oddil += dite
        dite.oddil = oddil

        if not dite.kamarad1 == -1:
            oddil += dite.kamarad1
            dite.kamarad1.oddil = oddil
            oddil.seznam_lidi_navic.append(dite.kamarad1)

        if not dite.kamarad2 == -1:
            oddil += dite.kamarad2
            dite.kamarad2.oddil = oddil
            oddil.seznam_lidi_navic.append(dite.kamarad2)
        return True, duvod
    else:
        return False, duvod


def find_triplet(seznam_deti, oddil, margin):
    if len(seznam_deti) == 0:
        return None
    for index, dite in enumerate(seznam_deti):
        if dite.oddil == - 1 and dite.pocet_kamaradu() == 2:
            pridat, vysvetleni = mam_pridat_dite_trojce_dvojce(oddil, dite, margin) # test jestli neni v seznamu deti navic nejaka shoda
            if pridat:
                return seznam_deti.pop(index)
    return None


def find_twins(seznam_deti, oddil, margin):
    for index, dite in enumerate(seznam_deti):
        if dite.oddil == - 1 and dite.pocet_kamaradu() == 1:
            pridat, vysvetleni = mam_pridat_dite_trojce_dvojce(oddil, dite,
                                                 margin)  # test jestli neni v seznamu deti navic nejaka shoda
            if pridat:
                return seznam_deti.pop(index)
    return None


def find_single(seznam_deti, oddil, margin):
    for index, dite in enumerate(seznam_deti):
        if dite.oddil == -1 and dite.pocet_kamaradu() == 0:
            pridat, vysvetleni = mam_pridat_dite(oddil, dite,
                                                 margin)  # test jestli neni v seznamu deti navic nejaka shoda
            if pridat:
                return seznam_deti.pop(index)
    return None

def neexistuje_dite_bez_oddilu(seznam_deti):
    for dite in seznam_deti:
        if dite.oddil == -1:
            return False
    return True

def najit_rozdeleni_b(pocet_deti, seznam_kapacit, result=[]):
    if pocet_deti == 0:
        return result

    for index, element in enumerate(seznam_kapacit):
        # to je kdyz jsme na konci seznamu
        if element[0] == 0:
            break
        # pokud je chatka k dispozici a neni vyhrazena pro holky, pouzit
        if element[1] > 0 and element[2] == 0 and element[0] <= pocet_deti:
            result.append(element[0])
            seznam_kapacit[index][1] -= 1
            vysledek = najit_rozdeleni_b(pocet_deti - element[0], seznam_kapacit, result)
            if vysledek is not None:
                return vysledek
            else:
                result.remove(element[0])
                seznam_kapacit[index][1] += 1
    return None

def najit_rozdeleni_g(seznam_kapacit):
    result = []
    for element in seznam_kapacit:
        while element[1] > 0:
            result.append((element[0], element[2]))
            element[1] -= 1
    return result

def vybalancovat_kapacity(list_kapacit_b, seznam_kapacit):
    pocet_osmicek = list_kapacit_b.count(8)
    # spocitat pocet ctyrek naivne
    pocet_ctyrek = 0
    for index, element in enumerate(seznam_kapacit):
        if element[0] == 4 and element[2] == 0:
            pocet_ctyrek += element[1]
    pocet_ctyrek = pocet_ctyrek // 2
    if pocet_ctyrek % 2 != 0:
        pocet_ctyrek -= 1
    while pocet_ctyrek >= 2:
        list_kapacit_b.remove(8)
        list_kapacit_b.append(4)
        list_kapacit_b.append(4)
        pocet_ctyrek -= 2
        for element in seznam_kapacit:
            if element[0] == 4 and element[2] == 0:
                element[1] -= 2
        for element in seznam_kapacit:
            if element[0] == 8 and element[2] == 0:
                element[1] += 1
    # return list_kapacit_b, seznam_kapacit












def increment_index_tam_a_zpet(index, delka_seznamu, otocka, smer_tam):
    if not smer_tam and index == 0 and not otocka:
        otocka = True
    elif not smer_tam and index == 0 and otocka:
        smer_tam = True
        index += 1
        otocka = False
    elif smer_tam and index == delka_seznamu - 1 and not otocka:
        otocka = True
    elif smer_tam and index == delka_seznamu - 1 and otocka:
        otocka = False
        smer_tam = False
        index -= 1
    elif smer_tam and index < delka_seznamu - 1:  # seznamem jedeme od 0 nakonec
        index += 1
    elif not smer_tam and index < delka_seznamu - 1:  # vracime se od konce na zacatek
        index -= 1
    else:
        otocka = False
        print("sem jsme se nechteli dostat, jsme u incrementovani indexu tam a zpet pro prochazeni oddilu")
    return index, otocka, smer_tam


def best_rating_house(rating_list):
    best_house = None
    best_rating = -1000
    for touple in rating_list:
        if touple[0] > best_rating:
            best_rating = touple[0]
            best_house = touple[1]
    return best_house


def evaluate_ratings(rating_list):
    if len(rating_list) == 0:
        return rating_list
    if rating_list[0][0] >= 0:
        index = 1
        while index < len(rating_list):
            if rating_list[index][0] == 0:
                rating_list.remove(rating_list[index])
            else:
                index += 1

    return rating_list

def naplnit_chatky_old(self, seznam_deti, treshold):
     # initialise a list of ratings
    rating_list = []
    # pick a kid without a house
    kid_without_house = seznam_deti.find_no_house_kid()
    # if kid_without_house.second_name == 'Smetana17':
        #   print ('hello')
    if kid_without_house is None:  # if there is no kid without house, we were sucessful. return
        return self
    for chatka in self.seznam_chatek:
        rating = 0
        if not chatka.chatka_is_full() and (chatka.pohlavi == "neurceno" or kid_without_house.gender == chatka.pohlavi): #the gender must be the same
            # compute how good fit it would be without adding the kid in
            # is kids oddil already in the house?
            oddily_in_house = chatka.chatka_oddily() # create a list of oddils in da house
            if len(oddily_in_house) == 0:
                rating += 0  # noone in the house, maybe little bad?
            elif len(oddily_in_house) == 1:
                if oddily_in_house.count(kid_without_house.oddil.cislo_oddilu) > 0:  # if there is already the same oddil as the kid is in
                    rating += 3  # this is good, increase the rating
                else:
                    rating += 1 # this is good
            else:
                if oddily_in_house.count(kid_without_house.oddil.cislo_oddilu) > 0:  # if there is already the same oddil as the kid is in
                    rating -= 2  # already 2 or more oddils, rating bad
                else:
                    rating -= 5

            # account for the age difference
            age_difference = abs(chatka.age_avg() - kid_without_house.age)
            if age_difference == kid_without_house.age:
                rating += 0
            elif age_difference <= 1:
                rating += 4
            else:
                rating -= int(age_difference) # the bigger the difference the bigger the penalty

            rating_list.append([rating, chatka]) # save the rating of the house

    #  the rating list from best to worse
    rating_list.sort(reverse=True)
    #evaluate ranking list so that only plausible choices remain
    rating_list = evaluate_ratings(rating_list.copy())

    if len(rating_list) == 0 or rating_list[0][0] < treshold: # if we didnt find any match, return false
        return False
    # pick the house with the best rating
    for index in range(len(rating_list)):
        best_house = rating_list[index][1]
        best_house += kid_without_house  # add the kid in the house
        # call the function again and try with the next kid
        result = self.naplnit_chatky(seznam_deti, treshold)
        if result is not False:  # if succes return
            return self
        else:
            best_house -= kid_without_house  # if failed, reset and try another house
    #if for all possibilities no succes, go another step back
    return False

        # try different housing compute how good it is and store all results in an array
        # choose the best result and call this function again
        # decide if the housing sequence was successful or not
        # if not try the second best housing option
'''
pocet_deti = 23
zasobnik_kapacit = [[0 for x in range(4)] for y in range (10)]
zasobnik_kapacit[0][0] = 8
zasobnik_kapacit[0][1] = 10
zasobnik_kapacit[1][0] = 5
zasobnik_kapacit[1][1] = 10
zasobnik_kapacit[2][0] = 4
zasobnik_kapacit[2][1] = 10

result, rozlozeni = jde_rozdelit(pocet_deti, zasobnik_kapacit, "")
print(result)
print(rozlozeni)
'''