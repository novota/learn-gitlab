
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 12:09:24 2019

@author: petrnovota
"""
import pandas as pd
from Hlavni_kod.Trida_chatka import Seznam_chatek
from Hlavni_kod.Trida_dite import Seznam_deti
from Hlavni_kod.Trida_oddil import Seznam_oddilu
import jinja2
import tkinter as tk
from tkinter.filedialog import askopenfilename
from tkinter import ttk

# TODO: pridat kontrolu, ze maji maximalne 3 deti stejny friends code

# globalni promene
global file_name
seznam_oddilu  = Seznam_oddilu()
max_lidi_v_oddile = -1
'''
# tkinter stuff
def end():
    main.destroy()

def OpenFile():
    global file_name
    file_name = askopenfilename(
                           filetypes =(("excel files", "*.xlsx"),("All Files","*.*")),
                           title = "nacti taborovy soubor"
                           )
    l1['text'] = 'cesta souboru je: ' + file_name
    #Using try in case user types in unknown file or closes without choosing a file.
    try:
        with open(file_name,'r') as UseFile:
            print(UseFile.read())
    except:
        print("No file exists")

def potvrd():
    global max_lidi_v_oddile
    max_lidi_v_oddile = entry1.get()
    try:
        max_lidi_v_oddile = int(max_lidi_v_oddile)
        l2['text'] = 'maximum je ' + str(max_lidi_v_oddile) + ' lidi v oddile'
    except:
        l2['text'] = 'neplatne cislo. zkus to znova'


def definuj_oddily():
    global seznam_oddilu
    l2['text'] = 'zadej celkovy pocet oddilu'
    entered_nr = potvrd()
    print(entered_nr)
'''



def vytvor_oddily():
    # import excelu with pandas

    seznam_xlsx = pd.read_excel(r'/Users/petrnovota/Documents/Programming/Tabory_rozrazovani_deti/r4_2020_tatari.xlsx')
    # print(seznam_xlsx)
    seznam_komplet = seznam_xlsx.loc[:, ('Jméno', 'R', 'M', 'D', 'S', 'Kód','Město')]
    seznam_komplet.fillna(-1, inplace=True)  # nahradi nan hodnoty -1kou
    # print(seznam_komplet)
    # print(seznam_komplet)
    seznam_deti_in_numpy = seznam_komplet.values
    # print(seznam_deti_in_numpy)

    offset = 0  # pocet dni od tabora od ktereho mam data na testovani, tak aby sedel vek taborniku k datu konani tabora
    seznamdeti = Seznam_deti().inicializace_seznamu_deti(seznam_deti_in_numpy, offset)

    # inicializace dat
    seznam_oddilu = Seznam_oddilu()
    seznam_oddilu.inicializace_oddilu(15, 9, 11)
    seznam_oddilu.inicializace_oddilu(14, 1, 20)

    # kontrola, ze mame dostatek mista pro vsechny pionyry?

    # prirazeni deti do oddilu
    margin = 0.4  # tweak smycku pres margin a vybrat rozlozeni s nejmensim rozptylem
    seznam_ruznych_oddilu = []
    seznam_oddilu.naplnit_oddily_na_kapitana_varianta(seznamdeti, margin)
    for oddil in seznam_oddilu.seznam_oddilu:
        oddil.seznam_deti.sort(key=lambda x: x.age, reverse=True)
    seznam_oddilu.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu)
    # print(seznam_oddilu)
    # vybrat prvnich 9 oddilu ???
    s = seznam_oddilu.seznam_oddilu[0:9]
    prvni_cast_oddilu = Seznam_oddilu(s)
    prvni_cast_deti = Seznam_deti()
    for oddil in prvni_cast_oddilu.seznam_oddilu:
        for dite in oddil.seznam_deti:
            prvni_cast_deti += dite

    # vytvor excel
    # prevod seznamu do pandas data frame
    # print(seznamdeti.vystup_2D_pro_excel())
    a1 = pd.DataFrame(seznamdeti.vystup_2D_pro_excel())
    # a1.style.hide_index() - nefunguje
    a1.to_excel("/Users/petrnovota/Documents/Programming/Tabory_rozrazovani_deti/r4_2020_tatari_oddily.xlsx")

    return seznamdeti, seznam_oddilu

def vytvor_ubytovani(seznam_deti):
    # serad deti podle veku od nejmensich po nejvetsi a podle oddilu
    seznam_deti.seznam_deti.sort(key=lambda x: (x.oddil.cislo_oddilu, x.age))
    # print(seznam_deti)

    # inicializace a ulozeni radova
    # seznam_Radov = Seznam_chatek("Novy Radov Hornak")
    # seznam_Radov.inicializace_chatek()
    # seznam_Radov.save()
    novy_seznam = Seznam_chatek("Novy_Radov_Hornak.csv").load()
    zkraceny_seznam = novy_seznam.cast_chatek(20)  # tweak tady muzu vybrat podskupinu chatek
    zkraceny_seznam.vytvorit_ubytovani(seznam_deti)
    print(zkraceny_seznam)
    # seznam_podseznamu, seznam_rozlozeni = novy_seznam.naplnit_chatky(seznam_deti, 4)
    print(seznam_deti)




#
# tkinter
'''
main = tk.Tk()
label = tk.Label(main, height='10', width='40', bg='white')


b1 = tk.Button(main, text="end", command=end, height='10', width='40')
b1.grid(row=0, rowspan=2, column=0, sticky='we')

b2 = tk.Button(main, text="open file", command=OpenFile, height='10', width='40')
b2.grid(row=2, rowspan=2, column=0, sticky='we')

#b3 = tk.Button(main, text="definuj oddily", command=definuj_oddily, height='10', width='40')
#b3.grid(row=4, rowspan=2, column=0, sticky='we')

b4 = tk.Button(main, text="vytvor oddily", command=vytvor_oddily, height='10', width='40')
b4.grid(row=6, rowspan=2, column=0, sticky='we')

b5 = tk.Button(main, text="potvrzuji", command=potvrd)
b5.grid(row=5, column=2, sticky='we')

l1 = tk.Label(main, text='klikni na open file a yvber cestu k souboru s taborniky')
l1.grid(row=2, column=1, sticky='we')
l2 = tk.Label(main, text='zadej maximalni pocet lidi v oddile a stiskni potvrd')
l2.grid(row=4, column=1, sticky='we')

entry1 = tk.Entry(main)
entry1.grid(row=4, column=2)





main.mainloop()
'''
seznam_deti, seznam_oddilu = vytvor_oddily()
vytvor_ubytovani(seznam_deti)
for oddil in seznam_oddilu.seznam_oddilu:
    oddil.urcit_pocet_chatek()
print(seznam_oddilu)



'''

#import excelu with pandas


seznam_xlsx = pd.read_excel (r'/Users/petrnovota/Documents/Programming/Tabory_rozrazovani_deti/r3_hornak.xlsx')
#print(seznam_xlsx)
seznam_komplet = seznam_xlsx.loc[:, ('Jméno', 'R', 'M', 'D', 'S', 'Kód')]
seznam_komplet.fillna(-1, inplace=True) #nahradi nan hodnoty -1kou
#print(seznam_komplet)
#print(seznam_komplet)
seznam_deti_in_numpy = seznam_komplet.values
#print(seznam_deti_in_numpy)

offset = 300   #pocet dni od tabora od ktereho mam data na testovani, tak aby sedel vek taborniku k datu konani tabora
seznamdeti = Seznam_deti().inicializace_seznamu_deti(seznam_deti_in_numpy, offset)

# inicializace dat
seznam_oddilu = Seznam_oddilu()
seznam_oddilu.inicializace_oddilu(15, 18, 1)


#kontrola, ze mame dostatek mista pro vsechny pionyry?

#prirazeni deti do oddilu
margin = 1  # tweak tento margin muzeme upravovat a dostaneme ruzne vekove prumery skupin
seznam_ruznych_oddilu = []
seznam_oddilu.naplnit_oddily_na_kapitana(seznamdeti, margin)
for oddil in seznam_oddilu.seznam_oddilu:
    oddil.seznam_deti.sort(key=lambda x: x.age, reverse=True)
seznam_oddilu.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu)
print(seznam_oddilu)
# vybrat prvnich 9 oddilu
s = seznam_oddilu.seznam_oddilu[0:9]
prvni_cast_oddilu = Seznam_oddilu(s)
prvni_cast_deti = Seznam_deti()
for oddil in prvni_cast_oddilu.seznam_oddilu:
    for dite in oddil.seznam_deti:
        prvni_cast_deti += dite


'''
'''
while margin < 1.5:
    # inicializace dat
    seznam_oddilu = Seznam_oddilu()
    seznam_oddilu.inicializace_oddilu(16, 10, 1)
    seznam_oddilu.naplnit_oddily_na_kapitana(seznamdeti, margin)
    nejvetsi_rozdil_vekoveho_prumeru = seznam_oddilu.vekovy_rozdil_min_max()
    seznam_ruznych_oddilu.append((nejvetsi_rozdil_vekoveho_prumeru,margin))
    margin += 0.1
    seznamdeti.odebrat_oddily()
seznam_ruznych_oddilu.sort(key=lambda x:x[0])
seznam_oddilu = Seznam_oddilu()
seznam_oddilu.inicializace_oddilu(15, 11, 1)
seznam_oddilu.naplnit_oddily_na_kapitana(seznamdeti, seznam_ruznych_oddilu[0][0])
'''
'''
prvni_cast_deti.seradit_sestupne()
seznamdeti.seradit_sestupne()
seznam_g = seznamdeti.seznam_holek()
seznam_b = seznamdeti.seznam_kluku()
#print(seznamdeti)

#naplnit chatky
# TODO: otestovat jestli pri dannem rozlozeni kamaradu v kazde skupine pujdou kamaradi rozhodit do navrhovanych chatek
# tohle nebude asi fungovat, udelat znova rekursivne jednoduse
#seznam_podseznamu, seznam_rozlozeni = zkraceny_seznam.naplnit_chatky(seznamdeti, 2) #cislo urcuje na kolik podseznamu rozdelime seznam
#print(seznam_podseznamu)
#print(seznam_rozlozeni)
prvni_cast_deti.seznam_deti.sort(key=lambda x: x.age, reverse=True)
prvni_cast_deti.seznam_deti.sort(key=lambda x: x.oddil.cislo_oddilu)

seznamdeti.seznam_deti.sort(key=lambda x: x.age, reverse=True)
seznamdeti.seznam_deti.sort(key=lambda x: x.oddil.cislo_oddilu)
zkraceny_seznam.obsadit(prvni_cast_deti, 0) # zkousim rozdelit tabor a deti na dve pulky  a rozdelit je po pulkach
novy_seznam.obsadit(seznamdeti, 0)

for oddil in seznam_oddilu.seznam_oddilu:
    oddil.seznam_deti.sort(key=lambda x: x.age, reverse=True)
    oddil.urcit_pocet_chatek()
seznam_oddilu.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu)
print(seznam_oddilu)
#print(zkraceny_seznam)


#vystupy
#print(seznam_oddilu)
#print(seznam_chatek)
#print(seznamdeti)

#prevod seznamu do pandas data frame
#print(seznamdeti.vystup_2D_pro_excel())
a1 = pd.DataFrame(seznamdeti.vystup_2D_pro_excel())
a1.to_excel("/Users/petrnovota/Documents/Programming/Tabory_rozrazovani_deti/output_R3_hornak.xlsx")



#seradit deti podle veku
###############################################################################
# hodnoty ktere bude potreba nastavit, nebo nechat program automaticky zkouset:
# mam_pridat_dite() margin
# naplnit_chatky() pocet_podseznamu
'''





